<?php
namespace Johary1634\Translation\service;

use GuzzleHttp\Client;
use stdClass;

class Translaty
{
    private string $userId;
    private string $projectId;
    private string $apiKey;
    private Client $client;
    private static $baseUri = "http://localhost:3002/";
    /**data storage */
    public array $languages = [];
    public stdClass $translate;
    public float $version = 0;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
        $this->client = new Client(["base_uri" => self::$baseUri]);
        $this->initiateData();
    }
    public function initiateData()
    {
        if (!file_exists(realpath("") . "/lang")) {
            mkdir(realpath("") . "/lang", 0777, true);
        }
        $response = $this->client->request('GET', "/services/translations/{$this->apiKey}");
        $data = json_decode($response->getBody()->getContents());
        $this->translate = $data->translations;
        $this->languages = $data->languages;
        $this->version = $data->version;
        $this->createJsonFile();
    }
    public function createJsonFile()
    {
        $arrayData = (array) $this->translate;
        $keys = array_keys($arrayData);
        // create json file
        foreach ($keys as $key) {
            $jsonData = json_encode($this->translate->$key, JSON_PRETTY_PRINT);
            file_put_contents(realpath("") . "//lang/" . $key . ".json", $jsonData);
        }

    }
    public function getVersion()
    {
        $response = $this->client->request('GET', "/services/versions/{$this->userId}");
        $version = json_decode($response->getBody()->getContents());
        if ($version != $this->version) {
            $this->version = $version;
        }
        return $version;
    }

    public function __toString()
    {
        return "apiKey = {$this->apiKey}";
    }
}
